import React from "react";
import { StackNavigator } from "react-navigation";
import LoginScreen from "../screen/LoginScreen";
import HomeScreen from "../screen/HomeScreen";

export default StackNavigator(
  {
    Login: {
      screen: LoginScreen
    },
    Home: {
      screen: HomeScreen
    }
  },
  {
    headerMode: "none"
  }
);
