import React, { Component } from "react";
import { View, Text, ToastAndroid, Dimensions } from "react-native";
import { GoogleSignin } from "react-native-google-signin";
import CustomButton from "../component/CustomButton/CustomButton";
import ESTyleSheet from "react-native-extended-stylesheet";

const { width, height } = Dimensions.get("window");
const customButtonWidth = 96;

class HomeScreen extends Component {
  render() {
    const user = this.props.navigation.state.params;
    const welcomeText = "Dobrodosao " + user.givenName + "!";
    return (
      <View style={styles.container}>
        <CustomButton
          title={"Odjavi se"}
          onPress={this.logoutPressed}
          style={styles.button}
        />
        <Text>{welcomeText}</Text>
      </View>
    );
  }

  logoutPressed = () => {
    GoogleSignin.signOut({ autoResolve: true })
      .then(() => {
        ToastAndroid.show("Uspjesno ste odjavljeni", ToastAndroid.LONG);
        this.props.navigation.goBack("");
      })
      .catch(err => {
        console.log("Play services error", err.code, err.message);
      });
  };
}

const styles = ESTyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "$windowBackground"
  },
  button: {
    position: "absolute",
    top: height - 100,
    left: width / 2 - customButtonWidth / 2
  },
  text: {
    color: "$primaryText",
    fontSize: 16
  }
});

export default HomeScreen;
