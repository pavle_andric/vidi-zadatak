import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { GoogleSignin } from "react-native-google-signin";
import CustomButton from "../component/CustomButton/CustomButton";
import ESTyleSheet from "react-native-extended-stylesheet";

class LoginScreen extends Component {
  componentWillMount() {
    GoogleSignin.currentUserAsync()
      .then(user => {
        if (user != null) {
          this.props.navigation.navigate("Home", user);
        }
      })
      .done();
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={require("./images/vidi_logo.png")}
        />
        <CustomButton title="Kreni" onPress={this.loginPressed} />
      </View>
    );
  }

  loginPressed = () => {
    GoogleSignin.hasPlayServices({ autoResolve: true })
      .then(() => {
        return GoogleSignin.configure();
      })
      .then(() => {
        return GoogleSignin.signIn();
      })
      .then(user => {
        console.log(user);
        this.props.navigation.navigate("Home", user);
      })
      .catch(err => {
        console.log("Play services error", err.code, err.message);
      });
  };
}

const styles = ESTyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "$white"
  },
  image: {
    width: 252,
    height: 85,
    marginBottom: 32
  }
});

export default LoginScreen;
