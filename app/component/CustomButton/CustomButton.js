import React, { Component } from "react";
import { TouchableOpacity, Text, View } from "react-native";
import PropTypes from "prop-types";
import styles from "./styles";

class CustomButton extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    style: PropTypes.any
  };

  render() {
    const { title, style } = this.props;
    return (
      <TouchableOpacity
        onPress={() => this.buttonPressed()}
        activeOpacity={0.8}
        style={[styles.container, style]}
      >
        <Text style={styles.title}> {title} </Text>
      </TouchableOpacity>
    );
  }

  buttonPressed() {
    console.log("custom button pressed");
    this.props.onPress();
  }
}

export default CustomButton;
