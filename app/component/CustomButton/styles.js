import ESTyleSheet from "react-native-extended-stylesheet";

export default ESTyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    height: 40,
    width: 96,
    borderRadius: 3,
    elevation: 3,
    backgroundColor: "$buttonBackground",
  },
  title: {
    fontSize: 16,
    fontWeight: "400",
    color: "$primaryText"
  }
});
