import React from "react";
import EStyleSheet from "react-native-extended-stylesheet";
import Navigator from "./config/routes";

EStyleSheet.build({
  $white: "#fff",
  $buttonBackground: "#CFD8DC",
  $windowBackground: "#F3F3F3",
  $primaryText: "#00000080"
});

export default () => <Navigator />;
